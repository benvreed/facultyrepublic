class AddLeadAuthorToPublications < ActiveRecord::Migration
  def change
    add_column :publications, :lead_author, :string
  end
end
