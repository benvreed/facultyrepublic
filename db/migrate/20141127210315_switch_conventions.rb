class SwitchConventions < ActiveRecord::Migration
  def change
      # rename_column :users, :USER_last, :last
      # rename_column :users, :USER_first, :first
      # rename_column :users, :USER_middle, :middle
      rename_column :types, :TYPE_Name, :name
      rename_column :titles, :TITLE_Name, :name
      rename_column :publications, :Title, :title
      rename_column :publications, :Lead_Author, :lead_author
      rename_column :publications, :Year, :year
  end
end
