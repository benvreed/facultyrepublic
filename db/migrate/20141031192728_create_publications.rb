class CreatePublications < ActiveRecord::Migration
  def change
    create_table :publications do |t|
      t.string :title
      t.integer :lead_author
      t.integer :type_id
      t.integer :year
      t.string :article
      t.timestamps
    end
  end
end
