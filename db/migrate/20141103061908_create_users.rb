class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :password_digest
      t.string :salt
      t.string :last
      t.string :first
      t.string :middle
      t.string :email
      t.integer :title_id

      t.timestamps
    end
  end
 def self.down
     drop_table :users
  end
end
