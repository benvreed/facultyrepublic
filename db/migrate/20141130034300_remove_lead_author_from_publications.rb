class RemoveLeadAuthorFromPublications < ActiveRecord::Migration
  def change
    remove_column :publications, :lead_author, :integer
  end
end
