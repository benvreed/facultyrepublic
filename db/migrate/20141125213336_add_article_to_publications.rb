class AddArticleToPublications < ActiveRecord::Migration
  def change
      add_column :publications, :article, :string
  end
end
