class SwitchJoinNameOnPublications < ActiveRecord::Migration
  def change
      rename_column :publications, :Type, :type_id
  end
end
