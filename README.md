# Faculty Republic

Beginning as a group project for a Principles of Software Engineering class,
Faculty Republic enables professionals to upload, collaborate, and build upon
research in their respective fields.  It promotes education and communication
in a proprietary world. 

The original problem comes down to this: Faculty members at Lipscomb University
needed a way to upload and store their publications.  Ideally, these
publications were designed to be shared with students and others interested in
the field. However, the Faculty Republic project not only meets these 
constraints, but it opens this data to the entire web.

## The Ideology
When somebody discovers something, it normally leads to something else.
Most of the time, it isn't the same person who discovers the later. However,
we live in a world where publications by scholars are restricted to certain
people.  This roadblock inhibits innovation and intellectual growth.  The
College of Computing and Informatics at Lipscomb University has decided
to break this barrier.  This way, anyone can have access to a God-given
right to learn.

## Caveats

Things you may note: 

* This project is actively in development; therefore, it may contain some
	bugs and other unpleasant issues.
	
* As of right now, the website authenticates against the Lipscomb University
	domain and an email validation link. Therefore, it is *currently* limited
	to this institution.  However, if you are interested, open an issue on
	the GitHub page and we'll work to accomodate your institution as well.

* The git repo does not contain all the needed code to run the Ruby on Rails
	application. This is due to security reasons.  You must create your own
	`config/database.yml` file and generate your own `config/secrets.yml` file.
	
* The students, and Lipscomb University, make no warranty for this software,
	it is distributed "AS IS."

## Contributing

Due to this application being a school project, unsolicited pull requests
cannot be accepted at this time. However, once significant progress is
made, we plan to accept these in the future.  Keep your eye out on the
repo -- primarily around December.

## Special Thanks

Faculty Republic is dedicated to the Computer Science staff at Lipscomb
University.  They encouraged, motivated, and provided servers for our
project to come to life.

Special thanks to our instructor, [Dr. Simmons](http://www.lipscomb.edu/technology/chris-simmons)
who led our Software Engineering class which inspired the project.
