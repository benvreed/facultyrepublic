Rails.application.routes.draw do

  # Our homepage is processed by the static controller's index action.
  root 'static#index'

  get 'logout', to:'sessions#destroy', as:'logout'
  get 'login', to:'sessions#new', as: 'login'
  get 'signup' => "users#new", :as => "signup"
  get 'users/confirm' => 'users#confirm'
  get 'users/invalid_request' => 'users#invalid_request'
  get 'users/forgot' => 'users#forgot'
  post 'users/reset' => 'users#reset'
  get 'users/reauth' => 'users#reauth'
  patch 'users/reauth_submit' => 'users#reauth_submit'
  #match '/auth/:provider/callback', :to => 'sessions#create',via: [:get, :post]
  #match '/auth/failure', :to => 'sessions#failure',via: [:get, :post]

  resources :publications

  #root to: 'application'
  resources :users

  resources :sessions
  #root to: 'sessions#new'
  #root 'sessions#create'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
