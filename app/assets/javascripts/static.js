$(document).ready(function () {
    if ($("#logo").attr('data-expanded') == "true") {
        $("#logo").replaceWith('<img alt="Logo" id="logo" src="/assets/logo.svg" style="width: 130px; height: 130px;">');
        var pastScroll = $(document).scrollTop();
        $(document).scroll(function () {
            if ($(document).scrollTop() > 55) {
              if (!(pastScroll > 55)) {
                $("#logo").replaceWith('<img alt="Logo" id="logo" src="/assets/logo_hover.svg" style="width: 50px; height: 50px;">');
                $("#slogan").hide();
              }
              pastScroll = $(document).scrollTop();
            }
            else {
              if (!(pastScroll < 55)) {
                $("#logo").replaceWith('<img alt="Logo" id="logo" src="/assets/logo.svg" style="width: 130px; height: 130px;">');
                $("#slogan").show();
              }
              pastScroll = $(document).scrollTop();
            }
        });
    }
    else {
        $("#logo").replaceWith('<img alt="Logo" id="logo" src="/assets/logo_hover.svg" style="width: 130px; height: 130px;">');
        $("#logo").css({
            "width": "50px",
            "height": "50px"
        });
        $("#slogan").hide();
    }
});
