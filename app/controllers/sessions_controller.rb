class SessionsController < ApplicationController

  def new
      if current_user.nil?
         render "new"
      else
         redirect_to publications_url, :notice => "You silly goose! You're already logged in."
      end
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to publications_url, :notice => "Logged in!"
    else
      flash.now.alert = "Invalid email or password"
      render "new"
    end
  end

  def destroy
  session[:user_id] = nil
  redirect_to root_url
  end
end
