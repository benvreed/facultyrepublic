class UsersController < ApplicationController
  def new
    @user = User.new
   end

   def create
     @user = User.new(user_params)
     @user.verified = false
     @user.confirmation_token = SecureRandom.urlsafe_base64(nil, false)
     if @user.save
        UserMailer.new_user_confirmation(@user).deliver
        session[:user_id] = @user.id
        redirect_to publications_url, :notice =>"Thank you for signing up! Please confirm your email."
     else
        render :new
     end
   end

   def show
     # show profile
     @user = User.find_by_id(params[:id])
   end

   def confirm
     user = User.find_by_confirmation_token(params[:token])
     if user.nil?
       redirect_to users_invalid_request_url, :notice => "Unfortunately, the token provided does not seem authentic."
     else
       user.verified = true
     end
   end

   def invalid_request

   end

   def forgot
     @user = User.new
   end

   def reset
     @user = User.find_by_email(params[:user][:email])
    #  raise @user
     if @user.nil?
       render "forgot", notice: "There is no user with that email."
     else
       @user.forgot_password_token = SecureRandom.urlsafe_base64(nil, false)
       if @user.save
         UserMailer.user_password_reset(@user).deliver
         redirect_to root_url
       end
     end
   end

   def reauth
     @user = User.find_by_forgot_password_token(params[:token])
     if @user.nil?
       redirect_to users_invalid_request_url, :notice => "Unfortunately, the token provided does not seem authentic."
     end
   end

   def reauth_submit
     @user = User.find_by_forgot_password_token(params[:user][:token])
      if @user.update(password_params)
         redirect_to publications_url, notice: 'Account was successfully updated.'
      else
        render "forgot"
     end
   end


private

   def user_params
       params.require(:user).permit(:password, :password_confirmation, :last, :first, :email, :article)
   end

   def password_params
     params.require(:user).permit(:password, :password_confirmation)
   end
end
