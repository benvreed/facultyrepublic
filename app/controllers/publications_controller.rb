class PublicationsController < ApplicationController
  before_action :force_authenticate
  before_action :set_publication, only: [:show, :edit, :update, :destroy]
  helper_method :sort_column, :sort_direction


  # GET /publications
  # GET /publications.json
  def index
    @search = Publication.order(sort_column+ ' ' + sort_direction).search do
      fulltext params[:search]
     end
  @publications = @search.results
  end

  # GET /publications/1
  # GET /publications/1.json
  def show
  end

  # GET /publications/new
  def new
    @publication = Publication.new
  end

  # GET /publications/1/edit
  def edit
  end

  # POST /publications
  # POST /publications.json
  def create
    @publication = Publication.new(publication_params)
    params[:publication][:user_ids].each do |user_id|
      if user_id != ""
        temp_user = User.find(user_id)
        @publication.users << temp_user
      end
    end
    respond_to do |format|
      if @publication.save
        format.html { redirect_to @publication, notice: 'Publication was successfully created.' }
        format.json { render :show, status: :created, location: @publication }
      else
        format.html { render :new }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /publications/1
  # PATCH/PUT /publications/1.json
  def update
    respond_to do |format|
      if @publication.update(publication_params)
        format.html { redirect_to @publication, notice: 'Publication was successfully updated.' }
        format.json { render :show, status: :ok, location: @publication }
      else
        format.html { render :edit }
        format.json { render json: @publication.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /publications/1
  # DELETE /publications/1.json
  def destroy
    @publication.destroy
    respond_to do |format|
      format.html { redirect_to publications_url, notice: 'Publication was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


   def sort_column
     Publication.column_names.include?(params[:sort]) ? params[:sort] : "Title"
   end
 
   def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
   end
    # Use callbacks to share common setup or constraints between actions.
    def set_publication
      @publication = Publication.find(params[:id])
    end

  private
  def sort_column
    Publication.column_names.include?(params[:sort]) ? params[:sort] : "Title"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_publication
    @publication = Publication.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def publication_params
    params.require(:publication).permit(:title, :article, :user_ids, :year)
  end
end
