class UserMailer < ActionMailer::Base
  add_template_helper(UserMailerHelper)
  default from: "postmaster@facultyrepublic.com"

  def new_user_confirmation(user)
    @user = user
    @token = user.confirmation_token
    mail(to: @user.email, subject: 'Action Required: Please Confirm your Email.')
  end

  def user_password_reset(user)
    @user = user
    @token = user.forgot_password_token
    mail(to: @user.email, subject: 'Action Required: Forgot Password Reset.')
  end
end
