class User < ActiveRecord::Base
#has_many :authorizations
#has_many :publishes
has_secure_password
validates_uniqueness_of :email
#attr_accessible :email, :password, :password_confirmation

validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  validates_presence_of :email
  validates_uniqueness_of :email
  validates_presence_of :first
  validates_presence_of :last

  has_many :authorships
  has_many :publications, through: :authorships
# def self.authenticate(email, password)
#   user = find_by_email(email)
#   if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
#     user
#   else
#     nil
#   end
# end


def full_name
  "#{first} #{last}"
end

#attr_accessible :email, :password, :password_confirmation
#validates :name, :email, :presence => true

#def self.create_with_omniauth(auth)
 # create! do |user|
  #  user.provider = auth["provider"]
   # user.uid = auth["uid"]
    #user.name = auth["user_info"]["name"]
   #end
  #end

#def add_provider(auth_hash)
  # Check if the provider already exists, so we don't add it twice
 # unless authorizations.find_by_provider_and_uid(auth_hash["provider"], auth_hash["uid"])
  #  Authorization.create :user => self, :provider => auth_hash["provider"], :uid => auth_hash["uid"]
  #end

end
