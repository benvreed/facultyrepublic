class Publication < ActiveRecord::Base
   searchable do
     text :title
   end
mount_uploader :article, ArticleUploader

has_many :authorships
has_many :users, through: :authorships

end
