json.array!(@publications) do |publication|
  json.extract! publication, :id, :Title
  json.url publication_url(publication, format: :json)
end
