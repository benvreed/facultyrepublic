module UserMailerHelper

  def user_confirmation_link(token)
    # this method should produce and return the appropriate link, given a token
    # for a user to confirm there email
    "http://facultyrepublic.com/users/confirm?token="+token
  end

  def user_password_reset_link(token)
    # this method should produce and return the appropriate link, given a token
    # for a user to reauthenticate after a lost password
    "http://facultyrepublic.com/users/reauth?token="+token
  end

end
