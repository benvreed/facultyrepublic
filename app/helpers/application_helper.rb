module ApplicationHelper
   def sortable(column, title = nil)
       title ||= column.titleize
       css_class = (column == sort_column) ? "current #{sort_direction}" : nil
       direction = (column == sort_column && sort_direction == "asc") ? "desc" : "asc"
       link_to title, {:sort => column, :direction => direction}, {:class => css_class}
   end

   def avatar_url(user)
     gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
     "http://gravatar.com/avatar/#{gravatar_id}.png?s=170"
   end

   def full_name(user)
     "#{user.first} #{user.last}"
   end

   def mailto_link(user)
     "<a href=\"mailto:#{user.email}\">#{user.email}</a>".html_safe
   end

   def pretty_publication_with_link(publication)
     link_to "#{publication.title}", publication
   end
end
